package no.ntnu.imt3281.project1;

import java.awt.Component;

import javax.swing.JDialog;


/**
 * TextArea class the holds information about text areas.
 *
 */
public class TextArea extends BaseComponent {
	private static final long serialVersionUID = 1L;
	private int rows;
	private int cols;
	private boolean wrap;
	private SpecialEditor editor;
	
	
	/**
	 * Constructor for the TextArea
	 */
	public TextArea() {
	}
	
	/**
	 * Constructor that takes another component as a argument
	 * @param component another BaseComponent 
	 */
	public TextArea(BaseComponent component) {
		super(component);
	}

	/**
	 * @return the boolean wrap value
	 */
	public boolean isWrap() {
		return wrap;
	}

	/**
	 * @param wrap the wrap to set
	 */
	public void setWrap(boolean wrap) {
		this.wrap = wrap;
	}
	
	/**
	 * @return the rows value
	 */
	public int getTextRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setTextRows(int rows) {
		this.rows = rows;
	}

	/**
	 * @return the TextCols value
	 */
	public int getTextCols() {
		return cols;
	}

	/**
	 * @param cols the TextCols to set
	 */
	public void setTextCols(int cols) {
		this.cols = cols;
	}
   
	/**
	 * Creating a editor for editing extra parameters for the object
	 */
	@Override
	public Component getSpecialEditor() {
		this.editor = new SpecialEditor(this.getClass().getSimpleName());
		
		// Setting the column, rows and wrap value of editor
		this.editor.setColsSpinner(cols);
		this.editor.setRowsSpinner(rows);
		this.editor.setWrapBox(wrap);
		
		return editor;
	}

	/**
	 * Triggers when the user click the OK button,
	 * stores the value from the width spinner, then
	 * running cancel to delete the dialog.
	 */
	public void editorOk(JDialog dialog) {
		this.cols = editor.getColsSpinner();
		this.rows = editor.getRowsSpinner();
		this.wrap = editor.getWrapBox();
		editorCancel(dialog);
	}

	/**
	 * Creates a definition with the name of the class, the variable name and the text
	 * @return String definition
	 */
	public String getDefinition() {
		return  "\tJ" + this.getClass().getSimpleName() + " " + this.getVariableName() + " = new J" 
				+ this.getClass().getSimpleName() + "(\"" + this.getText() + "\", " + rows  + ", " + cols + ");\n";
	}
	/**
	 * @return String layout code
	 */
	public String getLayoutCode() { 
		String layout = "\t\tgbc.gridx = " + this.getCol() + ";\n" 
				+ "\t\tgbc.gridy = " + this.getRow() + ";\n"
				+ "\t\tgbc.gridwidth = " + this.getCols() + ";\n" 
				+ "\t\tgbc.gridheight = " + this.getRows() + ";\n"
				+ "\t\tgbc.anchor = " + this.getAnchor() + ";\n" 
				+ "\t\tgbc.fill = " + this.getFill() + ";\n"
				+ "\t\tlayout.setConstraints(" + this.getVariableName() + ", gbc);\n"
				+ "\t\tadd(" + this.getVariableName() + ");\n";
		if(this.wrap) {
			layout += "\t\t" + this.getVariableName() + ".setWrapStyleWord(true);\n";
		} else {
			layout += "\t\t" + this.getVariableName() + ".setWrapStyleWord(false);\n";
		}
		return layout;
	}
}
