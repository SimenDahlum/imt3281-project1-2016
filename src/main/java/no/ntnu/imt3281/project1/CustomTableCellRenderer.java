package no.ntnu.imt3281.project1;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Showing the image or string for the objekt in the TableCell
 */
public class CustomTableCellRenderer extends JLabel implements TableCellRenderer {
	private static final long serialVersionUID = 1L;
	private Object[] items;
	private int[] values = null;
	/**
	 * Constructor that sets the items in the object
	 */ 
	public CustomTableCellRenderer(Object[] items) {
		setOpaque(true);
		this.items = items;
	}

	/**
	 * Constructor that sets bouth the items and the values in the object
	 */
	public CustomTableCellRenderer(Object[] items, int[] values) {
		setOpaque(true);
		this.items = items;
		this.values = values;
	}
	
	/**
	 * Setting the image to be shown in the table cell when the combo box is not showing
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		boolean hasFocus, int row, int column) {
		if (value instanceof Integer) {
			int index = 0;
			for (int i = 0; i < this.values.length; i++) {
				if ((int)value == this.values[i])
					index = i;
			}
			setIcon((ImageIcon) items[index]);
		} else if (value.toString() != null) {
			setText(value.toString());		
		}
		
		if (isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(table.getBackground());
		}   
		return this;
	  }
}