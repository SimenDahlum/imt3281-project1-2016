package no.ntnu.imt3281.project1;
import javax.swing.ImageIcon;

/**
 * 
 * @author Simen
 * Arrays for the dropdown menus and their content
 */
public class Globals {
	protected static final String[] types = new String[] { "JButton", "JLabel", "JTextArea", "JTextField" };
	protected static final ImageIcon[] fillImages = new ImageIcon[] { 
		new ImageIcon("gbleditor_icons/skaler_begge.png"),
		new ImageIcon("gbleditor_icons/skaler_horisontalt.png"),
		new ImageIcon("gbleditor_icons/skaler_ingen.png"),
		new ImageIcon("gbleditor_icons/skaler_vertikalt.png")
	};
	protected static final int[] fillValues = new int[] { 
		java.awt.GridBagConstraints.BOTH,
		java.awt.GridBagConstraints.HORIZONTAL,
		java.awt.GridBagConstraints.NONE,
		java.awt.GridBagConstraints.VERTICAL
	};
	protected static final ImageIcon[] anchorImages = new ImageIcon[] { 
		new ImageIcon("gbleditor_icons/anchor_center.png"),
		new ImageIcon("gbleditor_icons/anchor_northeast.png"),
		new ImageIcon("gbleditor_icons/anchor_east.png"),
		new ImageIcon("gbleditor_icons/anchor_southeast.png"),
		new ImageIcon("gbleditor_icons/anchor_south.png"),
		new ImageIcon("gbleditor_icons/anchor_southwest.png"),
		new ImageIcon("gbleditor_icons/anchor_west.png"),
		new ImageIcon("gbleditor_icons/anchor_northwest.png")   	
	};
	protected static final int[] anchorValues = new int[] {
		java.awt.GridBagConstraints.CENTER,
		java.awt.GridBagConstraints.NORTHEAST,
		java.awt.GridBagConstraints.EAST,
		java.awt.GridBagConstraints.SOUTHEAST,
		java.awt.GridBagConstraints.SOUTH,
		java.awt.GridBagConstraints.SOUTHWEST,
		java.awt.GridBagConstraints.WEST,
		java.awt.GridBagConstraints.NORTHWEST
	};
	private Globals(){	}
}
