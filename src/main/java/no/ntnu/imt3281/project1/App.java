package no.ntnu.imt3281.project1;

import javax.swing.JTable;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

/**
 * Main file for the application holding the frame, and main components. This
 * class have the main function to start the program.
 */
public class App extends JFrame {
	private GBLEDataModel datamodel;
	private Menu menuBar;
	private Toolbar toolbar;
	private JTable table;
	private PopupMenu popup;
	private static final long serialVersionUID = 1L;


	/**
	 * Main function starting the application
	 * 
	 * @param args
	 *            Command line arguments to start the program with
	 */
	public static void main(String[] args) {  
		
		if (args.length == 2) {
			I18N.setLanguageCountry(args[0], args[1]);
		} else if (args.length == 1) {
			I18N.setLanguage(args[0]);
		}
		
		App application = new App();
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.setSize(900, 400); 
		application.setVisible(true); 

	}
	/**
	 * Constructor for main application. The constructor is private and will
	 * only be run once by main function when the application is starting.
	 */
	private App() {
		datamodel = new GBLEDataModel();
		datamodel.addComponent(new Button());
		datamodel.addComponent(new TextArea());
		datamodel.addComponent(new TextField());
		datamodel.addComponent(new Label());

		table = new JTable();

		MouseListener listener = new PopupListener();
		table.addMouseListener(listener);
		
		
		toolbar = new Toolbar();
		add(toolbar, "North");
		// tool bar listeners
		toolbar.getNewBtn().addActionListener(e -> createNewFile());
		toolbar.getOpenBtn().addActionListener(e -> loadFile());
		toolbar.getSaveBtn().addActionListener(e -> saveToFile());
		toolbar.getJavaBtn().addActionListener(e -> generateJava());
		toolbar.getAddBtn().addActionListener(e -> createNewComponent());
		toolbar.getUpBtn().addActionListener(e -> moveRowUp(-1));
		toolbar.getDownBtn().addActionListener(e -> moveRowDown(-1));


		menuBar = new Menu();
		setJMenuBar(menuBar);	
		// menu bar listeners
		menuBar.getNewItem().addActionListener(e -> createNewFile());
		menuBar.getOpenItem().addActionListener(e -> loadFile());
		menuBar.getSaveItem().addActionListener(e -> saveToFile());
		menuBar.getJavaItem().addActionListener(e -> generateJava());
		menuBar.getExitItem().addActionListener(e -> exitWindow());
		menuBar.getNewRowItem().addActionListener(e -> createNewComponent());
		menuBar.getRowUpItem().addActionListener(e -> moveRowUp(-1));
		menuBar.getRowDownItem().addActionListener(e -> moveRowDown(-1));
		
	
		setDataModel(datamodel);
		add(new JScrollPane(table));

	}

	private void setDataModel(GBLEDataModel newDataModel) {

		this.datamodel = newDataModel;
		table.setModel(newDataModel);

		TableColumn col = table.getColumnModel().getColumn(0);
		TableColumn anc = table.getColumnModel().getColumn(7);
		TableColumn fill = table.getColumnModel().getColumn(8);

		col.setCellEditor(new CustomCellEditor(Globals.types));
		col.setCellRenderer(new CustomTableCellRenderer(Globals.types));

		anc.setCellEditor(new CustomCellEditor(Globals.anchorImages,
				Globals.anchorValues));
		anc.setCellRenderer(new CustomTableCellRenderer(Globals.anchorImages,
				Globals.anchorValues));

		fill.setCellEditor(new CustomCellEditor(Globals.fillImages,
				Globals.fillValues));
		fill.setCellRenderer(new CustomTableCellRenderer(Globals.fillImages,
				Globals.fillValues));
	}

	/**
	 * Creates a new file and removes existing data
	 */
	private void createNewFile() {
		datamodel.clear();
		datamodel.fireTableDataChanged();
	}

	/**
	 * Loads data from file
	 */
	private void loadFile() {

		File file = null;

		JFileChooser loadChooser = new JFileChooser();
		int returnVal = loadChooser.showOpenDialog(App.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = loadChooser.getSelectedFile();

			try {
				datamodel.load(Files.newInputStream(Paths.get(file.getAbsolutePath())));
			} catch (IOException ioException) {
				System.err.println("Error reading from file. Terminating");
			}
		}
	}

	/**
	 * Saving data to file
	 */
	private void saveToFile() {

		File file = null;

		JFileChooser saveChooser = new JFileChooser();
		int returnVal = saveChooser.showSaveDialog(App.this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = saveChooser.getSelectedFile();
			try {
				datamodel.save(Files.newOutputStream(Paths.get(file.getAbsolutePath())));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Generate java code from data
	 */
	private void generateJava() {
		new Generator(datamodel);
	}

	/**
	 * Create a new component, and add it to the data model
	 */
	private void createNewComponent() {
		datamodel.addComponent(new Button());
		table.updateUI();
	}
	
	private void deleteSelectedRow(int row){
		if (row > 0)
			row = table.getSelectedRow();
		datamodel.removeComponent(row);
		table.updateUI();
	}

	/**
	 * Move the row down
	 */
	private void moveRowDown(int row) {
		if (row > 0)
			row = table.getSelectedRow();
		if (datamodel.moveComponentDown(row)) {
			table.setRowSelectionInterval(row + 1, row + 1);
			table.repaint();
		}
	}

	/**
	 * Move the row up
	 */
	private void moveRowUp(int row) {
		if (row > 0)
			row = table.getSelectedRow();
		if (datamodel.moveComponentUp(row)) {
			table.setRowSelectionInterval(row - 1, row - 1);
			table.repaint();
		}
	}

	/**
	 * Display a new dialog with the editor if the currently selected row is 
	 * a text area or a text field
	 */
	private void showSpecialEditor(int row) {
		JFrame frame = new JFrame();
		JDialog dialog = new JDialog(frame, "title", true);

		SpecialEditor editor;
		BaseComponent component = datamodel.getComponentAtIndex(row);
		if (component instanceof TextArea)
			editor = (SpecialEditor) ((TextArea) component).getSpecialEditor();
		else if (component instanceof TextField)
			editor = (SpecialEditor) ((TextField) component).getSpecialEditor();
		else
			editor = (SpecialEditor) component.getSpecialEditor();
		
		if (editor != null) {
			// Creating listeners for the buttons in the editor
			editor.getOkBtn().addActionListener(e -> component.editorOk(dialog));
			editor.getCancelBtn().addActionListener(e -> component.editorCancel(dialog));
		
			dialog.getContentPane().add(editor);
		
			// Making the dialog the size of the editor
			dialog.pack();
		
			// Adding 50 to the height and width of the dialog box 
			// to get some margin on the sides
			int width = dialog.getWidth() + 50;
			int height = dialog.getHeight() + 50;
			dialog.setSize(width, height);
			dialog.setLocation(100, 100);
			dialog.setVisible(true);
		}
	}
	/**
	 * 
	 * @author Simen
	 * Will create a new popup menu
	 * adds actionListener to the items in the menu
	 * Displays the correct items in the menu depending on where the users clicks
	 * two mouse checks because computers registers mouse clicks at different times, either release or pressed
	 */
	class PopupListener extends MouseAdapter{
		@Override
		public void mousePressed(MouseEvent e) {
			showPoppup(e);
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			showPoppup(e);    
		}
		public void showPoppup(MouseEvent e) {

			if (e.isPopupTrigger()) {
				int row = table.rowAtPoint(e.getPoint());
				String type = (String)datamodel.getValueAt(row, 0);
				
				boolean showEditor = false;
				boolean showUp = true;
				boolean showDown = true;

				if ( type.equals("JTextArea") || type.equals("JTextField")) {
					showEditor = true;
				}
				if (row == 0) {
					showUp = false;
				}
				if (datamodel.getRowCount() < row+2) {
					showDown = false;
				}
				popup = new PopupMenu(showUp, showDown, showEditor);
				popup.getMoveUp().addActionListener(ev -> moveRowUp(row));
				popup.getMoveDown().addActionListener(ev -> moveRowDown(row));
				popup.getDelete().addActionListener(ev -> deleteSelectedRow(row));
				popup.getEditor().addActionListener(ev -> showSpecialEditor(row));
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}

	}

	/**
	 * Exit program
	 */
	private void exitWindow() {
		System.exit(0);
	};



}
