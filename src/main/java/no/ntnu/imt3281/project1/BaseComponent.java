package no.ntnu.imt3281.project1;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.io.Serializable;

import javax.swing.JDialog;

/** 
 * Base Component used in Button, Label, TextArea and TextField
 */
public class BaseComponent implements Serializable {
	private static final long serialVersionUID = 1L;
	private static int nextComponentID;
	private String variableName;
	private String text;
	private int row;
	private int col;
	private int rows;
	private int cols;
	private int anchor;
	private int fill;

	/**
	 * Constructor for base component with no arguments.
	 */
	public BaseComponent() {
		this.row = 1;
		this.col = 1;
		this.rows = 1;
		this.cols = 1;
		this.anchor = GridBagConstraints.CENTER;
		this.fill = GridBagConstraints.NONE;
		this.variableName = "component" + nextComponentID;
		this.text = "";
		nextComponentID++;
	}
	
	/**
	 * Constructor taking another BaseComponent as an argument
	 * @param component another component
	 */
	public BaseComponent(BaseComponent component) {
		this.row = component.getRow();
		this.col = component.getCol();
		this.rows = component.getRows();
		this.cols = component.getCols();
		this.variableName = component.getVariableName();
		this.anchor = component.getAnchor();
		this.fill = component.getFill();
		this.text = component.getText();
	}
	
	/**
	 * @return component
	 */
	public Component getSpecialEditor() {
		return null;
	}
	
	/**
	 * Creates a definition with the name of the class, the variable name and the text
	 * @return String definition
	 */
	public String getDefinition() {
		return  "\tJ" + this.getClass().getSimpleName() + " " + this.getVariableName() + " = new J" 
				+ this.getClass().getSimpleName() + "(\"" + this.getText() + "\");\n";
	}
	
	/**
	 * @return String layout code
	 */
	public String getLayoutCode() {
		return   "\t\tgbc.gridx = " + col + ";\n" 
				+ "\t\tgbc.gridy = " + row + ";\n"
				+ "\t\tgbc.gridwidth = " + cols + ";\n" 
				+ "\t\tgbc.gridheight = " + rows + ";\n"
				+ "\t\tgbc.anchor = " + anchor + ";\n" 
				+ "\t\tgbc.fill = " + fill + ";\n"
				+ "\t\tlayout.setConstraints(" + variableName + ", gbc);\n"
				+ "\t\tadd(" + variableName + ");\n";
	}
	
	/**
	 * @return the variableName
	 */
	public String getVariableName() {
		return variableName;
	}
	
	/**
	 * @param variableName the variableName to set
	 */
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
	
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}
	
	/**
	 * @return the number of collumns
	 */
	public int getCol() {
		return col;
	}
	
	/**
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}
	
	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}
	
	/**
	 * @param rows the rows to set
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	/**
	 * @return the cols
	 */
	public int getCols() {
		return cols;
	}
	
	/**
	 * @param cols the cols to set
	 */
	public void setCols(int cols) {
		this.cols = cols;
	}
	
	/**
	 * @return the anchor index
	 */
	public int getAnchor() {
		return anchor;
	}
	
	/**
	 * @param anchor the anchor to set
	 */
	public void setAnchor(int anchor) {
		this.anchor = anchor;
	}
	
	/**
	 * @return the fill index
	 */
	public int getFill() {
		return fill;
	}
	
	/**
	 * @param fill the fill to set
	 */
	public void setFill(int fill) {
		this.fill = fill;
	}
	
	/**
	 * @return the nextComponentID
	 */
	public static int getNextComponentID() {
		return nextComponentID;
	}
	
	/**
	 * @param nextComponentID the nextComponentID to set
	 */
	public static void setNextComponentID(int nextComponentID) {
		BaseComponent.nextComponentID = nextComponentID;
	}

	public Component getSpecialEdior() {
		return null;
	}

	/**
	 * Triggers when the user click cancel or by the OK function
	 * Will delete the dialog box
	 */
	public void editorCancel(JDialog dialog) {
		dialog.setVisible(false);
		dialog.dispose();
	}
	/**
	 * Running the cancel dialog function in case the ok button in called and the
	 * component do not have any special properties
	 */
	public void editorOk(JDialog dialog) {
		editorCancel(dialog);
	}

}
