package no.ntnu.imt3281.project1;

import javax.swing.ImageIcon; 
import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 * Object contains the tool bar so give the user options on the top of the screen.
 */
public class Toolbar extends JToolBar {
	private static final long serialVersionUID = 1L;
	private JButton newBtn;
	private JButton openBtn;
	private JButton saveBtn;
	private JButton javaBtn;
	private JButton addBtn;
	private JButton upBtn;
	private JButton downBtn;
	
	private String newFile = I18N.getBundle().getString("Toolbar.newFile");
	private String openFile = I18N.getBundle().getString("Toolbar.openFile");
	private String saveFile = I18N.getBundle().getString("Toolbar.saveFile");
	private String javaFile = I18N.getBundle().getString("Toolbar.genJavaCode");
	private String newRow = I18N.getBundle().getString("Toolbar.addRow");
	private String moveRowUp = I18N.getBundle().getString("Toolbar.moveRowUp");
	private String moveRowDown = I18N.getBundle().getString("Toolbar.moveRowDown");
	
	/**
	 * Constructor for the tool bar. 
	 * Creating the buttons and adding them to the tool bar
	 */
	Toolbar() {
		newBtn = createButton("NEW.GIF", newFile);
		openBtn = createButton("OPENDOC.GIF", openFile);
		saveBtn = createButton("SAVE.GIF", saveFile);
		javaBtn = createButton("SAVEJAVA.GIF", javaFile);
		addBtn = createButton("NEWROW.GIF", newRow);
		upBtn = createButton("MoveRowUp.GIF", moveRowUp);
		downBtn = createButton("MoveRowDown.GIF", moveRowDown);
 
		add(newBtn); 
		add(openBtn);
		add(saveBtn);
		addSeparator();
		add(javaBtn);
		addSeparator();
		add(addBtn);
		add(upBtn);
		add(downBtn);
		addSeparator();

	}
	
	/**
	 * Function to create a new button for the tool bar
	 * @param image the name of the image in the recourse folder
	 * @param text the tool tip text displayed on hover
	 * @return the new button object to add to the tool bar
	 */
	private JButton createButton(String image, String text) {
		JButton button = new JButton(new ImageIcon("gbleditor_icons/" + image));
		button.setToolTipText(text);
		return button;
	}
	
	/**
	 * @return the newBtn
	 */
	public JButton getNewBtn() {
		return newBtn;
	}

	/**
	 * @return the openBtn
	 */
	public JButton getOpenBtn() {
		return openBtn;
	}

	/**
	 * @return the saveBtn
	 */
	public JButton getSaveBtn() {
		return saveBtn;
	}

	/**
	 * @return the javaBtn
	 */
	public JButton getJavaBtn() {
		return javaBtn;
	}

	/**
	 * @return the addBtn
	 */
	public JButton getAddBtn() {
		return addBtn;
	}

	/**
	 * @return the upBtn
	 */
	public JButton getUpBtn() {
		return upBtn;
	}

	/**
	 * @return the downBtn
	 */
	public JButton getDownBtn() {
		return downBtn;
	}

}
