package no.ntnu.imt3281.project1;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Top menu for the application. Have a dropdown for file and edit with 
 * buttons to modify the datamodel
 */
public class Menu extends JMenuBar {
	private static final long serialVersionUID = 1L;
		
	//Initializing JMenu Items
	private JMenuItem newItem; 
	private	JMenuItem openItem;
	private	JMenuItem saveItem; 
	private	JMenuItem javaItem;
	private	JMenuItem exitItem;
	private	JMenuItem newRowItem;
	private	JMenuItem rowUpItem;
	private	JMenuItem rowDownItem;

		
	// Internationalized text
	private String menuFileText = I18N.getBundle().getString("Menu.menuFile");
	private String menuEditText = I18N.getBundle().getString("Menu.menuEdit");
	private String menuNewText = I18N.getBundle().getString("Menu.menuNew");
	
	private String menuLoadText = I18N.getBundle().getString("Menu.menuLoad");
	private String menuSaveText = I18N.getBundle().getString("Menu.menuSave");
	private String menuGenJavaCodeText = I18N.getBundle().getString("Menu.menuGenJavaCode");
	private String menuExitText = I18N.getBundle().getString("Menu.menuExit");
	
	private String menuNewRowText = I18N.getBundle().getString("Menu.menuNewRow");
	private String menuRowUpText = I18N.getBundle().getString("Menu.menuRowUp");
	private String menuRowDownText = I18N.getBundle().getString("Menu.menuRowDown");
    
    /**
     * Creating a new menu with buttons.
     */
	public Menu() {
		// Defining file-, edit- and help- Menu drop downs
		JMenu fileMenu = new JMenu(menuFileText);
		JMenu editMenu = new JMenu(menuEditText);
		
		// Creating menu items for the file menu
		newItem  = new JMenuItem (menuNewText, new ImageIcon("gbleditor_icons/NEW.GIF"));
		openItem = new JMenuItem(menuLoadText, new ImageIcon("gbleditor_icons/OPENDOC.GIF"));
		saveItem = new JMenuItem(menuSaveText, new ImageIcon("gbleditor_icons/SAVE.GIF"));
		exitItem = fileMenu.add(menuExitText);
		javaItem = new JMenuItem(menuGenJavaCodeText, new ImageIcon("gbleditor_icons/SAVEJAVA.GIF"));
		
		// Adding file menu items to the file menu
		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.addSeparator();
		fileMenu.add(javaItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		
		// Creating menu items for the edit menu
		newRowItem = new JMenuItem(menuNewRowText, new ImageIcon("gbleditor_icons/NEWROW.GIF"));
		rowUpItem = new JMenuItem(menuRowUpText, new ImageIcon("gbleditor_icons/MoveRowUp.GIF"));
		rowDownItem = new JMenuItem(menuRowDownText, new ImageIcon("gbleditor_icons/MoveRowDown.GIF"));

		// Adding the menu items to the edit menu
		editMenu.add(newRowItem);
		editMenu.add(rowUpItem);
		editMenu.add(rowDownItem);
		
		//Adding file-,edit- and helpMenu to the UI menu bar
		add(fileMenu);
		add(editMenu);
		
	}
	/**
	 * @return the menu item for new file
	 */
	public JMenuItem getNewItem() {
		return newItem;
	}


	/**
	 * @return the menu item for open file
	 */
	public JMenuItem getOpenItem() {
		return openItem;
	}

	/**
	 * @return the menu item for saving file
	 */
	public JMenuItem getSaveItem() {
		return saveItem;
	}

	/**
	 * @return the menu item for generating java
	 */
	public JMenuItem getJavaItem() {
		return javaItem;
	}

	/**
	 * @return the menu item for exiting
	 */
	public JMenuItem getExitItem() {
		return exitItem;
	}

	/**
	 * @return the menu item for creating a new row
	 */
	public JMenuItem getNewRowItem() {
		return newRowItem;
	}


	/**
	 * @return the menu item for moving a row up
	 */
	public JMenuItem getRowUpItem() {
		return rowUpItem;
	}

	/**
	 * @return the menu item for moving row down
	 */
	public JMenuItem getRowDownItem() {
		return rowDownItem;
	}

	
}
