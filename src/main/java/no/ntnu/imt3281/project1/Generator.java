package no.ntnu.imt3281.project1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.sun.xml.internal.ws.util.StringUtils;

/**
 * Generator used to generate code out from from the objects in the data model
 * 
 * @author Lasse Sviland
 */
public class Generator {
	/**
	 * Constructor will ask the user for a file, then get the file content from
	 * getFole and write it to the file using writeTextFile
	 * 
	 * @param model
	 *            the data model to generate the code from
	 */
	Generator(GBLEDataModel model) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("JAVA files",
				".java"));
		int returnVal = fileChooser.showSaveDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			writeTextFile(getFileContents(model, file), file.getAbsolutePath());
		}
	}

	/**
	 * Creating the full code out from the data model and the file name using
	 * the filename as the class name
	 * 
	 * @param model
	 *            the data model with the information about objects
	 * @param file
	 *            the filename the user selected
	 * @return a list of string to have in the file
	 */
	private ArrayList<String> getFileContents(GBLEDataModel model, File file) {
		ArrayList<String> text = new ArrayList<>();
		String className = StringUtils.capitalize(file.getName().split(
				Pattern.quote("."))[0]);
		text.add("import javax.swing.*;");
		text.add("import java.awt.*;\n");
		text.add("public class " + className + " extends JPanel {");
		text.add(model.getDefinitions());
		text.add("\tpublic " + className + "() {");
		text.add("\t\tGridBagLayout layout = new GridBagLayout ();");
		text.add("\t\tGridBagConstraints gbc = new GridBagConstraints();");
		text.add("\t\tsetLayout (layout);");
		for (BaseComponent component : model.getData())
			text.add(component.getLayoutCode());
		text.add("\t}");
		text.add("\tpublic static void main(String[] args) {");
		text.add("\t\tJFrame frame = new JFrame();");
		text.add("\t\tframe.setSize(800, 800);");
		text.add("\t\tframe.setVisible(true);");
		text.add("\t\tframe.add(new " + className + "());");
		text.add("\t}");
		text.add("}");
		return text;
	}

	/**
	 * Takes a list of strings to insert into a text file
	 * 
	 * @param text
	 *            the list of strings
	 * @param filePath
	 *            the full path to the file location
	 */
	private static void writeTextFile(ArrayList<String> text, String filePath) {
		try {
			FileOutputStream fos = new FileOutputStream(filePath);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			for (String line : text) {
				bw.write(line);
				bw.newLine();
			}
			bw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
