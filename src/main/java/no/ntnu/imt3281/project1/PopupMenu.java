package no.ntnu.imt3281.project1;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * This will handle the right clicks on the rows in the table
 * @author Simen
 * 
 */
public class PopupMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;

	private String[] popupItems = { I18N.getBundle().getString("Popup.moveUp"), 
									I18N.getBundle().getString("Popup.moveDown"),
									I18N.getBundle().getString("Popup.delete"),
									I18N.getBundle().getString("Popup.editor")};
	
	private JMenuItem moveUp;
	private JMenuItem moveDown;
	private JMenuItem delete;
	private JMenuItem editor;
	/**
	 * Adds the needed options in the menu based on the row and cell
	 * @param showUp, if the row can be moved up
	 * @param showDown, if the row can be moved down
	 * @param showEditor, if the cell should show the special editor
	 */
	PopupMenu(boolean showUp, boolean showDown, boolean showEditor){
		moveUp = new JMenuItem(popupItems[0]);
		moveDown = new JMenuItem(popupItems[1]);
		delete = new JMenuItem(popupItems[2]);
		editor = new JMenuItem(popupItems[3]);
		
		if(showUp) {
			add(moveUp);	
		}
		if(showDown) {
			add(moveDown);
		}
		add(delete);
		if(showEditor) {
			add(editor);
		}	
	}
	/**
	 * @return moveUp menuItem
	 */
	public JMenuItem getMoveUp(){
		return moveUp;
	}
	/**
	 * @return moveDown menuItem
	 */
	public JMenuItem getMoveDown(){
		return moveDown;
	}
	/**
	 * @return delete menuItem
	 */
	public JMenuItem getDelete(){
		return delete;
	}
	/**
	 * @return editor menuItem
	 */
	public JMenuItem getEditor(){
		return editor;
	}
}
