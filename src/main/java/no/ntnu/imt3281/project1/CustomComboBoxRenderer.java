package no.ntnu.imt3281.project1;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Creating JLabel for the elements in the combo box
 */
public class CustomComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {
	private static final long serialVersionUID = 1L;
	private Object[] items;
	private int[] values = null;

	/**
	 * Constructor setting the items in the object
	 * @param items a list of strings to display in the drop down
	 */
	public CustomComboBoxRenderer(Object[] items) {
		this.items = items;
		setOpaque(true);
	}

	/**
	 * Constructor setting the items and values in the object
	 * @param items a list of images to display in the drop down
	 * @param values a the values that the images will represent
	 */
	public CustomComboBoxRenderer(Object[] items, int[] values) {
		this.items = items;
		this.values  = values;
		setOpaque(true);
	}
  
  /**
   * Returning the component used in the cell in the combo box
   * @return a JLabel that will have the value to display in the combo box
   */
	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		setOpaque(true);

		if (isSelected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		if (values != null) {
			int imageIndex = Integer.parseInt(value.toString());
			for (int i = 0; i < this.values.length; i++) {
				if (imageIndex == this.values[i]) {
					return new JLabel((ImageIcon)items[i]);
				}
			}
		}
			
		return new JLabel((String)value.toString());
	}
}
