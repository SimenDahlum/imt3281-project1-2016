package no.ntnu.imt3281.project1;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * Editor that asks the user for additional information for a base component
 */
public class SpecialEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private JLabel heading = new JLabel("La stå 0 for å ikke benytte parameteren");
	  
	// TEXT AREA
	private JLabel rowsLabel = new JLabel("rows");
	private JLabel colsLabel = new JLabel("cols");
	private JLabel wrapLabel = new JLabel("wrap");
	private JSpinner rowsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 1));
	private JSpinner colsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 1));
	private JCheckBox wrapBox = new JCheckBox();
	  
	// TEXT FIELD
	private JLabel widthLabel = new JLabel("width");
	private JSpinner widthSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 1));
	  
	private JButton okBtn = new JButton("OK");
	private JButton cancelBtn = new JButton("avbryt");
	  
	  
	/**
	 * Creating a new editor with width for text fields and 
	 * rows, columns and wrap check box for text areas
	 * @param type the type of editor, TextField or TextArea
	 */
	public SpecialEditor(String type) {
		// Header taking 3 columns
		GridBagLayout layout = new GridBagLayout();
		setLayout(layout);
		GridBagConstraints constrains = getConstrains(1,1);
		constrains.gridwidth = 3;
		layout.setConstraints(heading, constrains);
		add(heading);
		
		if(type.equals("TextField")) {
			// Width
			layout.setConstraints(widthLabel, getConstrains(1,2));
			add(widthLabel);
			layout.setConstraints(widthSpinner, getConstrains(2,2));
			add(widthSpinner);

		} else if (type.equals("TextArea")) {
			// Rows
			layout.setConstraints(rowsLabel, getConstrains(1,2));
			add(rowsLabel);
			layout.setConstraints(rowsSpinner, getConstrains(2,2));
			add(rowsSpinner);
			
			// Columns
			layout.setConstraints(colsLabel, getConstrains(1,3));
			add(colsLabel);
			layout.setConstraints(colsSpinner, getConstrains(2,3));
			add(colsSpinner);
			
			// Wrap content
			layout.setConstraints(wrapLabel, getConstrains(1,4));
			add(wrapLabel);
			layout.setConstraints(wrapBox, getConstrains(2,4));
			add(wrapBox);
		}

		// Cancel / OK Buttons
		layout.setConstraints(cancelBtn, getConstrains(2,5));
		add(cancelBtn);
		layout.setConstraints(okBtn, getConstrains(3,5));
		add(okBtn);
	}

	/**
	   * Creating a constrains object out from the parameters sent with the call.
	   * will always place elements in west.
	   * @param x the column in the grid
	   * @param y the row in the grid
	   * @return the constrains object that was created
	   */
	private GridBagConstraints getConstrains(int x, int y) {
		GridBagConstraints constrains = new GridBagConstraints();
		constrains.gridx = x;
		constrains.gridy = y;
		constrains.anchor = GridBagConstraints.WEST;
		return constrains;
	}
	  
	/**
	 * @return the value of the width spinner
	 */
	public int getWidthSpinner() {
		return (Integer) widthSpinner.getValue();
	}
	  
	/**
	 * @return the value of the row spinner
	 */
	public int getRowsSpinner() {
		return (Integer) rowsSpinner.getValue();
	}
	  
	/**
	 * @return the value of the column spinner
	 */
	public int getColsSpinner() {
		return (Integer)colsSpinner.getValue();
	}
	  
	/**
	 * @return the value of the wrap select box
	 */
	public boolean getWrapBox() {
		return wrapBox.isSelected();
	}
	  
	/**
	 * Setting the value of the with spinner
	 * @param width the value to set
	 */
	public void setWidthSpinner(int width) {
		widthSpinner.setValue(width);
	}
	  
	/**
	 * Setting the value of the rows spinner
	 * @param rows the value to set
	 */
	public void setRowsSpinner(int rows) {
		rowsSpinner.setValue(rows);
	}
	  
	/**
	 * Setting the value of the column spinner
	 * @param cols the value to set
	 */
	public void setColsSpinner(int cols) {
		colsSpinner.setValue(cols);
	}
	  
	/**
	 * Setting the checked property of the wrap check box
	 * @param value the value to set
	 */
	public void setWrapBox(boolean value) {
		wrapBox.setSelected(value);
	}
	  
	/**
	 * @return the OK button
	 */
	public JButton getOkBtn() {
	   return okBtn;
	}
	  
	/**
	 * @return the cancel button
	 */
	public JButton getCancelBtn() {
		return cancelBtn;
	}
}
