package no.ntnu.imt3281.project1;

import java.awt.Component;

import javax.swing.JDialog;


/**
 * Class for TextFields, having a width variable, and a own editor
 */
public class TextField extends BaseComponent {
	private static final long serialVersionUID = 1L;
	private int width;
	private SpecialEditor editor;
	
	/**
	 * Constructor for the textField
	 */
	public TextField() {
	}
	
	/**
	 * Constructor that takes another BaseComponent as an argument
	 * @param component another base component
	 */
	public TextField(BaseComponent component) {
		super(component);
	}
	
	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	
	/**
	 * Creates a definition with the name of the class, the variable name and the text
	 * @return the definition of the text field
	 */
	public String getDefinition() {
		return  "\tJ" + this.getClass().getSimpleName() + " " + this.getVariableName() + " = new J" 
				+ this.getClass().getSimpleName() + "(\"" + this.getText() + "\", " + width + ");\n";
	}
	
	/**
	 * Creating a editor for editing extra parameters for the object
	 */
	@Override
	public Component getSpecialEditor() {
		this.editor = new SpecialEditor(this.getClass().getSimpleName());
		
		// Setting the width value of the spinner
		this.editor.setWidthSpinner(width);

		
		return this.editor;
	}

	/**
	 * Triggers when the user click the OK button,
	 * stores the value from the width spinner, then
	 * running cancel to delete the dialog.
	 */
	public void editorOk(JDialog dialog) {
		this.width = editor.getWidthSpinner();
		editorCancel(dialog);
	}

}
