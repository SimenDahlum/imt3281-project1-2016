package no.ntnu.imt3281.project1;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;
/**
 * GBLEDataModel storing data that should be in the JTable. 
 * The data is BaseComponents that is each representing a row in the table.
 *
 */
public class GBLEDataModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private Vector<BaseComponent> data;
	
	private String[] columnName = { I18N.getBundle().getString("GBLEDataModel.typeColumn"),
									I18N.getBundle().getString("GBLEDataModel.nameColumn"),
									I18N.getBundle().getString("GBLEDataModel.textColumn"),
									I18N.getBundle().getString("GBLEDataModel.row"),
									I18N.getBundle().getString("GBLEDataModel.column"),
									I18N.getBundle().getString("GBLEDataModel.rows"),
									I18N.getBundle().getString("GBLEDataModel.columns"),
									I18N.getBundle().getString("GBLEDataModel.anchor"),
									I18N.getBundle().getString("GBLEDataModel.fill")};
	
	/**
	 * Creating new vector with the rows in the table
	 */
	public GBLEDataModel() {
		data = new Vector<BaseComponent>();
	}

	/**
	 * @return the data
	 */
	public Vector<BaseComponent> getData() {
		return data;
	}
	
	/**
	 * @return the length of the data set
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}
	
	/**
	 * Makes the cells in the table editable
	 * @return true
	 */
	public boolean isCellEditable(int row, int col) {
		return true;
	}
	
	/**
	 * @return the amount of columns in the table
	 */
	@Override
	public int getColumnCount() {
		return columnName.length;
	}
	
	/**
	 * returns the name of a column at a certain index
	 * @param the index of the column
	 * @return the column name
	 */
	@Override
	public String getColumnName(int column) {
		return columnName[column];
	}
	
	/**
	 * Fining the value that should be displayed in a certain cell in the table based on the column index and the row index.
	 * @param the index of the row
	 * @param the index of the column
	 * @return A object to display in the row
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		BaseComponent component = data.get(rowIndex);
		switch (columnIndex) {
			case 0:  return "J" + component.getClass().getSimpleName();
			case 1:  return component.getVariableName();
			case 2:  return component.getText();
			case 3:  return component.getRow();
			case 4:  return component.getCol();
			case 5:  return component.getRows();
			case 6:  return component.getCols();
			case 7:  return component.getAnchor();
			case 8:  return component.getFill();
		default:
			return "";
		}
	}
	
	/**
	 * Overriding the setValueAt function. Setting the value of a field in the table, 
	 * updating the variables the classes based on the changes
	 * @param aValue the new value the field was changed to
	 * @param rowIndex the index of the row that was changed
	 * @param columnIndex the index of the column that was changed
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		BaseComponent component = data.get(rowIndex);
		String strValue = aValue.toString();
		int intValue;
		try {
			intValue = Integer.parseInt(aValue.toString().replaceAll("[\\D]", ""));
		} catch (NumberFormatException e) {
			intValue = 0;
		}
			
		switch (columnIndex) {
			case 0:  changeComponentType(rowIndex, strValue); break;
			case 1:  component.setVariableName(strValue); break;
			case 2:  component.setText(strValue);   break;
			case 3:  component.setRow(intValue);    break;
			case 4:  component.setCol(intValue);    break;
			case 5:  component.setRows(intValue);   break;
			case 6:  component.setCols(intValue);   break;
			case 7:  component.setAnchor(intValue); break;
			case 8:  component.setFill(intValue);   break;
		default:
			component.setText(strValue);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	/**
	 * Changing the type of object the BaseComponent is based on
	 * @param rowIndex the index of the component to change
	 * @param componentType the new type of the component as a string
	 */
	public void changeComponentType(int rowIndex, String componentType) {
		BaseComponent oldComponent = data.get(rowIndex);
		BaseComponent newComponent;
		switch (componentType) {
			case "JButton":  newComponent = new Button(oldComponent);break;
			case "JLabel":  newComponent = new Label(oldComponent);break;
			case "JTextArea":  newComponent = new TextArea(oldComponent);break;
			case "JTextField":  newComponent = new TextField(oldComponent);break;
		default:
			newComponent = new Button(oldComponent);;
		}
		data.setElementAt(newComponent, rowIndex);
	}
	
	/**
	 * @return the definitions
	 */
	public String getDefinitions() {
		String definition = "";
		for (BaseComponent component : data)
			definition += component.getDefinition();
		return definition;
	}
	
	/**
	 * @return the layout code
	 */
	public String getLayoutCode() {
		return null;
	}
	
	/**
	 * @param bos the output stream
	 */
	public void save(OutputStream bos) {
		ObjectOutputStream output;
		try {
			output = new ObjectOutputStream(bos);
			output.writeObject(data);
			output.close();
		
		} catch (IOException e) {
			
		}
			
	}
	
	/**
	 * @param inputStream the input stream
	 */
	@SuppressWarnings("unchecked")
	public void load(InputStream inputStream) {	
		ObjectInputStream input;
		try {
			input = new ObjectInputStream(inputStream);
			this.data = (Vector<BaseComponent>) input.readObject();
			input.close();
			
		} 
		catch (IOException e) {
			
		} 
		catch (ClassNotFoundException e) {
			
		}
	}
		
	/**
	 * Removing all elements from the vector of rows in the table.
	 */
	public void clear() {
		data.removeAllElements();
	}

	/**
	 * Adding a component to the vector of rows
	 * @param component to add
	 */
	public void addComponent(BaseComponent component) {
		data.add(component);
	}
	
	/**
	 * Removing the component passed as a argument from the vector
	 * @param component to be removed
	 */
	public void removeComponent(BaseComponent component) {
		data.remove(component);
	}
	
	/**
	 * Removing component from the vector at index i
	 * @param i index to remove at
	 */
	public void removeComponent(int compIndex) {
		data.remove(compIndex);
	}
	
	/**
	 * Moving down component number i in the vector if i is not 0
	 * and there is more than one element in the vector
	 * @param compIndex index of component to move down
	 */
	public boolean moveComponentDown(int compIndex) {
		if (getRowCount() > compIndex + 1) {	
			BaseComponent component = data.remove(compIndex);
			data.add(compIndex + 1, component);
			return true;
		}
		return false;
	}
	
	/**
	 * Moving up component number i in the vector if there is 
	 * a element above the given index
	 * @param compIndex index of component to move up
	 */
	public boolean moveComponentUp(int compIndex) {
		if (compIndex > 0 ) {	
			BaseComponent component = data.remove(compIndex);
			data.add(compIndex - 1, component);
			return true;
		}
		return false;
	}
	
	/**
	 * returning the BaseComponent at the specified index
	 * @param index the index of the base component in the vector
	 * @return the base component at the index
	 */
	public BaseComponent getComponentAtIndex(int index) {
		return data.elementAt(index);
	}

}
