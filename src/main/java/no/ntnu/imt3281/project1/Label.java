package no.ntnu.imt3281.project1;


/**
 * Label class to hold information about a Label component
 */
public class Label extends BaseComponent {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for the Label
	 */
	public Label() {
	}
	
	/**
	 * Constructor for Label that takes a base component as a parameter
	 * @param component base component
	 */
	public Label(BaseComponent component) {
		super(component);
	}
	


}
