package no.ntnu.imt3281.project1;

import javax.swing.DefaultCellEditor;

public class CustomCellEditor extends DefaultCellEditor {
	private static final long serialVersionUID = 1L;

	public CustomCellEditor(Object[] items) {
		super(new CustomComboBox(items));
	}
	public CustomCellEditor(Object[] items, int[] values) {
		super(new CustomComboBox(items, values));
	}
}
