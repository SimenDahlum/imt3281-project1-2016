package no.ntnu.imt3281.project1;

import javax.swing.JComboBox;

/**
 * Custom combo box that will have the values for the images, and the strings for the types
 */
public class CustomComboBox extends JComboBox<Object> {
	private static final long serialVersionUID = 1L;
  
  /**
   * Constructor that takes items, will have the item as the valu
	 * @param items a list of items for the dispaly alternatives in the dropdown
   */
	public CustomComboBox(Object[] items) {
		for (int i = 0; i < items.length; i++) {
			addItem(items[i]);
		}
		setRenderer(new CustomComboBoxRenderer(items));
	}

	/**
	 * Constructor that takes items and values
	 * using the values for the value
	 * @param items a list of items for the dispaly alternatives in the dropdown
	 * @param values the value for the display value
	 */
	public CustomComboBox(Object[] items, int[] values) {
		for (int i = 0; i < items.length; i++) {
			addItem(values[i]);
		}
		setRenderer(new CustomComboBoxRenderer(items, values));
	}
}