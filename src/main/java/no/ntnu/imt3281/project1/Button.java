package no.ntnu.imt3281.project1;


/**
 * Class for Button components
 */
public class Button extends BaseComponent {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for the Button
	 */
	public Button() {
	}

	/**
	 * Constructor for the button that takes another base 
	 * component as a parameter
	 * @param component another base component
	 */
	public Button(BaseComponent component) {
		super(component);
	}


}
