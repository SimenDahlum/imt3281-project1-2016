package no.ntnu.imt3281.project1;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class will handle the i18n
 * @author oivindk, copied from your i18n, small changes for our program
 *
 */

public class I18N {
	/**
	 * Variable is not used, however it stores the instance, and prevents it from beeing deleted
	 */
	@SuppressWarnings("unused")
	private static I18N i18n = new I18N();
	private static ResourceBundle bundle;

    /**
     * Constructor sets English to default language
     */
    private I18N() {

		Locale newLocale = new Locale("en", "US");
		Locale.setDefault(newLocale);

		I18N.bundle = ResourceBundle.getBundle("no.ntnu.imt3281.project1.i18n",
				Locale.getDefault());
	}

	/**
	 * Change language
	 * @param language, sets the language
	 */
	public static void setLanguage(String language) {
		Locale locale = new Locale(language);
 
		I18N.bundle = ResourceBundle.getBundle("no.ntnu.imt3281.project1.i18n", locale);
 
	}

	/**
	 * Change language and country
	 * @param language, set the language
	 * @param country, set the country
	 */
	public static void setLanguageCountry(String language, String country) {
		Locale locale = new Locale(language, country);
		I18N.bundle = ResourceBundle.getBundle("no.ntnu.imt3281.project1.i18n",
				locale);
	}

	/**
	 * Get resource bundle
	 * 
	 * @return ResourceBundle for application
	 */
	public static ResourceBundle getBundle() {
		return bundle;
	}	
}
